# Book Application - Yaraku Assignment
This is an assignment provided by Yaraku to a candidate for gauging a candidate's knowledge. This is part of the interview process. 

## Tech Stack
> Php, Mysql
> Laravel
> Vuejs

### Installation and Setup
* Before setup please ensure that you have **LAMP** or **XAMP** setup on your machine.
* Checkout your branch.
* To install all dependencies, run
> composer install
* Change the DB credentials in the **.env** file and Run 
> php artisan migrate
* To setup all npm modules run
> npm install
* To build all modules run
> npm run dev
* To build any new code changes on the fly run
> npm run watch
* You can seed the table by running the following command
> php artisan db:seed --class=BookSeeder
* You can obtain the list of routes (apis) by running
> php artisan routes:list

### Information on Files
* **BooksApiController** is the location for all APIs
* **BooksController** has the csv and xml download code
* Vue components can be found in **resources/js/components**

### Contribution guidelines
* Ensure that your code is well documented
* Ensure your code is well indented
* Ensure you have followed other guidelines that your team has set

### Who do I talk to?
Repo owner - Krishnan - krishnan.ubuntu@gmail.com


