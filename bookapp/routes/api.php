<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('books')->group(function () {
    Route::post('add', 'App\Http\Controllers\BooksController@addNewBook');
    Route::get('all', 'App\Http\Controllers\BooksController@allBooks');
    Route::post('delete', 'App\Http\Controllers\BooksController@deleteBook');
    Route::post('update/author', 'App\Http\Controllers\BooksController@updateAuthorName');
    Route::get('search', 'App\Http\Controllers\BooksController@searchBooksAndAuthors');
    Route::get('sort', 'App\Http\Controllers\BooksController@sortBooks');
});