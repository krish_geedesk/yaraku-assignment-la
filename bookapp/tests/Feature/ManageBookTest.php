<?php

/**
* Test for Manage Books APIs like update, delete
*
* @author Krishnan krishnan.ubuntu@gmail.com
* 
*/
namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ManageBookTest extends TestCase
{


    //Test to check if the Books API fetches the books and returns as Json
    public function testGetBooksApi()
    {
        $response = $this->getJson('/api/books');

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 'success',
            ]);
    }

    
    //Test to check if the Delete Book API 
    public function testDeleteBook()
    {
        $response = $this->deleteJson('/api/books/delete/97');

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 'success',
            ]);
    }



    //Test to check if the Delete Book API works fine in case of an invalid Book ID
    public function testDeleteBookInvalidId()
    {
        $response = $this->deleteJson('/api/books/delete/10036');

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 'failed',
            ]);
    }


    //Test to check the Delete Book behavior without a Book ID
    public function testDeleteBookSansId()
    {
        $response = $this->deleteJson('/api/books/delete');

        $response
            ->assertStatus(404);
    }


    //Test to check the Update Book API
    public function testUpdateBookAuthor()
    {
        $response = $this->putJson('/api/books/update/30', ['author' => 'Kelly']);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 'success'
            ]);
    }



    //Test to check the Update Book API in the event of empty author value
    public function testUpdateBookAuthorEmptyVal()
    {
        $response = $this->putJson('/api/books/update/30', ['author' => '']);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 'failed'
            ]);
    }


    //Test to check the Update Book API in the event of no author value
    public function testUpdateBookTitle()
    {
        $response = $this->putJson('/api/books/update/4', ['title' => 'Kelly']);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 'failed',
            ]);
    }


    //Test to check the Update Book API if the author has a null value
    public function testUpdateBookTitleWithAuthorNull()
    {
        $response = $this->putJson('/api/books/update/4', ['title' => 'Kelly', 'author' => '']);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 'failed',
            ]);
    }


}
