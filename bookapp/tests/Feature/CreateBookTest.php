<?php

/**
* Test for Create Books API
*
* @author Krishnan krishnan.ubuntu@gmail.com
* 
*/
namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateBookTest extends TestCase
{
    
    //Test to check if the Create Books API is able to create book
    public function testBooksCreate()
    {
        $response = $this->postJson('/api/books/create', ['title' => 'Sally', 'author' => 'Jolly']);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 'success',
            ]);
    }


    //Test to check if the Create Books API doesn't create the book in case of invalid column names
    public function testBooksCreateInvalidParams()
    {
        $response = $this->postJson('/api/books/create', ['titles' => 'John', 'authors' => 'Nash']);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 'failed',
            ]);
    }


    //Test to check if the Create Books API doesn't create the book in case of empty /null values
    public function testBooksCreateEmptyVals()
    {
        $response = $this->postJson('/api/books/create', ['title' => '', 'author' => '']);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 'failed',
            ]);
    }
    
}
