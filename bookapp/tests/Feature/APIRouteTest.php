<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class APIRouteTest extends TestCase
{
    
    //Test to check if the home page is loading
    public function testHomePage()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }


}
