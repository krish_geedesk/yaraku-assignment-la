<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class BookSeeder extends Seeder
{
    /**
     * Run the database seeds. This seeder will insert 10 random values
     * 
     * @author Krishnan krishnan.ubuntu@gmail.com
     *
     * @return void
     */
    public function run()
    {
    	$counter = 0;

    	while ($counter <= 10) 
    	{
    		$counter = $counter + 1;

    		DB::table('books')->insert([
	            'title' => Str::random(10),
	            'author' => Str::random(10),
	        ]);
    	}
    }

}
