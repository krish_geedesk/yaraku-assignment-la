<?php

/**
* Books API
* 
* @author Krishnan krishnan.ubuntu@gmail.com
* 
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Book;
use App\Http\Resources\Book as BookResource;

use Response;
use DB;

class BooksApiController extends Controller
{

    /**
     * Display a list of books.
     * 
     * @author Krishnan krishnan.ubuntu@gmail.com
     * 
     * @return List of all the books from the books table
     *
     */
    public function index()
    {
        $books = Book::all();

        if (!empty($books)) 
        {
            return response()->json(['status' => 'success', 'books' => $books], 200);
        }
        else
        {
            return response()->json(['status' => 'failed', 'message'=> 'No books found'], 200);
        }
    }




    /**
     * Create a new book
     * 
     * @param $request (Request obtained as json from Ajax call)
     * 
     * @author Krishnan krishnan.ubuntu@gmail.com
     * 
     * @return Success or failure message
     *
     */
    public function store(Request $request)
    {
        if ($request->has('title') && $request->has('author')) 
        {
            if ($request->post('title') != '' && $request->post('author') != '') 
            {
                $create = Book::create($request->all());

                if ($create) 
                {
                      return response()->json(['status' => 'success', 'success' => true, 'message'=> 'Book created successfully'], 200);
                }
                else 
                {
                    return response()->json(['status' => 'failed', 'message'=> 'There was an error, please try again'], 200);
                }
            }
            else 
            {
                return response()->json(['status' => 'failed', 'message'=> 'Invalid request'], 200);
            }
        }
        else
        {
            return response()->json(['status' => 'failed', 'message'=> 'Invalid request'], 200);
        }
    }




    /**
     * Edit the existing book (only author can be updated)
     * 
     * @param $request (Request obtained as json from Ajax call) and $id (Book ID)
     * 
     * @author Krishnan krishnan.ubuntu@gmail.com
     * 
     * @return Success or failure message
     *
     */
    public function update(Request $request, $id)
    {
        
        $book = Book::find($id);

        if ($request->has('author')) 
        {
            if ($request->post('author') != '' && $request->post('author') != null) 
            {
                $book->author = $request->post('author');

                $update = $book->save();

                if ($update) 
                {
                      return response()->json(['status' => 'success', 'success' => true, 'message'=> 'Book author updated successfully'], 200);
                }
                else 
                {
                    return response()->json(['status' => 'failed', 'message'=> 'There was an error, please try again'], 200);
                }
            }
            else 
            {
                return response()->json(['status' => 'failed', 'message'=> 'Author cannot be empty'], 200);
            }
        }
        else 
        {
            return response()->json(['status' => 'failed', 'message'=> 'Invalid request'], 200);
        }
    }



    /**
     * Delete a book from the Book table
     * 
     * @param $id (Book ID)
     * 
     * @author Krishnan krishnan.ubuntu@gmail.com
     * 
     * @return Success or failure message
     *
     */
    public function destroy($id)
    {
        $book = Book::find($id);

        if ($book != '' && $book != null) 
        {
            $delete = Book::destroy($id);

            if ($delete) {
                  return response()->json(['status' => 'success', 'success' => true, 'message'=> 'Book deleted successfully'], 200);
            }
            else 
            {
                return response()->json(['status' => 'failed', 'error'=> 'There was an error, please try again'], 200);
            }
        }
        else 
        {
            return response()->json(['status' => 'failed', 'error'=> 'Invalid request'], 200);
        }
    }



    /**
     * Search a book based on the query
     * 
     * @param $request (query) - has the query based on which the books have to searched
     * 
     * @author Krishnan krishnan.ubuntu@gmail.com
     * 
     * @return List of all the books based on the search query
     *
     */
    public function search(Request $request)
    {
        $data = $request->get('query');

        $books = Book::where('title', 'like', "%{$data}%")
                         ->orWhere('author', 'like', "%{$data}%")
                         ->get();

        return $books;
    }



    /**
     * Fetch and sort the books list in asc or desc order of titles
     * 
     * @param $request (sort) ENUM (asc/desc) - sort is the paramater based on which the books or fetched
     * 
     * @author Krishnan krishnan.ubuntu@gmail.com
     * 
     * @return List of all the books based in asc or desc order
     *
     */
    public function sortByTitles(Request $request)
    {
        $sort = $request->get('sort');

        $sql_query = 'select id, title, author from books order by title '.$sort;

        $books = DB::select($sql_query);

        return $books;
    }



    /**
     * Fetch and sort the books list in asc or desc order of authors
     * 
     * @param $request (sort) ENUM (asc/desc) - sort is the paramater based on which the books or fetched
     * 
     * @author Krishnan krishnan.ubuntu@gmail.com
     * 
     * @return List of all the books based in asc or desc order
     *
     */
    public function sortByAuthors(Request $request)
    {
        $sort = $request->get('sort');

        $sql_query = 'select id, title, author from books order by author '.$sort;

        $books = DB::select($sql_query);

        return $books;
    }



} //class ends
